package edu.ifsp.lojinha.web;

import edu.ifsp.lojinha.modelo.Usuario;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpSessionEvent;
import jakarta.servlet.http.HttpSessionListener;

@WebListener
public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		Usuario usuario = (Usuario)se.getSession().getAttribute("usuario");
		System.out.println("Fim da sessão. Usuário: " + usuario.getUsername());
	}
}
