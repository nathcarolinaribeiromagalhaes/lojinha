<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Lojinha do Zé</title>
</head>

<body>
	<h1>Lojinha do Zé</h1> 

<c:choose>
	<c:when test="${not empty sessionScope.usuario}">
		<p>Olá, ${sessionScope.usuario.nome}! (<a href="logout">Sair</a>) </p>
	</c:when>
	
	<c:otherwise>
		<p><a href="login">Login</a></p>
	</c:otherwise>
</c:choose>
	
	<p>Faça <a href="cadastro.html">aqui</a> o seu cadastro de cliente!<p>
	
<c:if test="${not empty sessionScope.usuario}">
	<p><a href="listar.sec">Listar clientes</a></p>
</c:if>

</body>
</html>