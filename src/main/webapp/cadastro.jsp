<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="edu.ifsp.lojinha.modelo.Cliente" %>

<%
Cliente cliente = (Cliente)request.getAttribute("cliente");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Cadastro</title>
</head>
<body>
<p>Cadastro realizado com sucesso!</p>
<p>
Nome: <%= cliente.getNome() %><br>
E-mail: <%= cliente.getEmail() %>
</p>
</body>
</html>